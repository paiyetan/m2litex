/*
  *     Copyright (c) 2015, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 * 
 */

package m2litex;

import java.io.*;
import java.util.LinkedList;
import utilities.OneDirChooser;

/**
 *
 * @author paiyeta1
 */
public class M2LiteX {

    private LinkedList<String> uniquePeptides;
    private LinkedList<String> uniqueProteins;
    private int psms;
    private File[] files;

    public M2LiteX(File[] files) {
        this.files = files;
        this.psms = 0;
        this.uniquePeptides = new LinkedList<String>();
        this.uniqueProteins = new LinkedList<String>();       
    }
    
    private void summarizeIdentifications(boolean includeFileSummary) throws FileNotFoundException, IOException {
        //throw new UnsupportedOperationException("Not yet implemented");
        // at default FDR (High confidence == 3)
        retrieveUniquePeptides(includeFileSummary); //get unique Peptides
        retrieveUniqueProteins(includeFileSummary); //get unique Proteins
    }
    
    private void retrieveUniquePeptides(boolean includeFileSummary) throws FileNotFoundException, IOException {
        //throw new UnsupportedOperationException("Not yet implemented");
        System.out.println("  Retrieving unique peptides...");
        int count = 0;
        for(File file : files){
            count++;
            System.out.println("    File "+ count + ": " + file.getName());       
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            int linesRead = 0;
            while((line = reader.readLine())!=null){
                linesRead++;
                if(linesRead > 1){
                    String[] lineArr = line.split("\t");
                    if(lineArr[0].equalsIgnoreCase("3")){
                        psms++;
                        String sequence = lineArr[3];
                        if(uniquePeptides.contains(sequence)==false)
                           uniquePeptides.add(sequence); 
                    }
                }
            }
            reader.close();
        }
    }

    private void retrieveUniqueProteins(boolean includeFileSummary) throws FileNotFoundException, IOException {
        System.out.println("  Retrieving unique proteins...");
        int count = 0;
        for(File file : files){
            count++;
            System.out.println("    File "+ count + ": " + file.getName());       
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            int linesRead = 0;
            while((line = reader.readLine())!=null){
                linesRead++;
                if(linesRead > 1){
                    String[] lineArr = line.split("\t");
                    if(lineArr[0].equalsIgnoreCase("3")){
                        //psms++;
                        String prots = lineArr[25];
                        String[] protsArr = prots.split(";");
                        for(String prot : protsArr){
                            String[] protArr = prot.split("\\|");
                            String pro = protArr[1];
                            if(uniqueProteins.contains(pro)==false)
                            uniqueProteins.add(pro); 
                        }
                    }
                }
            }
            reader.close();
        }
    }

    

    private void printIdentificationSummary(String dir) throws FileNotFoundException {
        //throw new UnsupportedOperationException("Not yet implemented");
        String out = dir + File.separator + "pDIdentifications.summary";
        PrintWriter printer = new PrintWriter(out);
        printer.println("   Number of files: " + files.length);
        printer.println("    Number of PSMs: " + psms);
        printer.println("   Unique peptides: " + uniquePeptides.size());
        printer.println("   Unique proteins: " + uniqueProteins.size());               
        printer.close();
        
        out = dir + File.separator + "pDIdentifications.peptides";
        printer = new PrintWriter(out);
        for(String peptide : uniquePeptides)
            printer.println(peptide);
        printer.close();
        
        out = dir + File.separator + "pDIdentifications.proteins";
        printer = new PrintWriter(out);
        for(String protein : uniqueProteins)
            printer.println(protein);
        printer.close();       
        
    }
    
       
    public static void main(String[] args) throws FileNotFoundException, IOException{
        System.out.println("Starting...");
        
        // load search result dir....
        OneDirChooser chooser = new OneDirChooser("Select search out-files location (directory)...");
        String dir = chooser.getInputFile();
        File[] files = new File(dir).listFiles();
        
        // load output folder...
        chooser = new OneDirChooser("Select ouput directory...");
        dir = chooser.getInputFile();
        
        System.out.println("Summarizing....");
        M2LiteX spdids = new M2LiteX(files); // instatiate summarizer
        spdids.summarizeIdentifications(false); // summarize identifications
        
        System.out.println("Printing summary results....");
        spdids.printIdentificationSummary(dir); // print summary...
                
        System.out.println("....Done!!!");
        
    }

    
}
